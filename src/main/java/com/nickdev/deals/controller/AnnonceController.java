package com.nickdev.deals.controller;

import com.nickdev.deals.model.Annonce;
import com.nickdev.deals.model.Secteur;
import com.nickdev.deals.service.AnnonceService;
import com.nickdev.deals.service.SecteurService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Data
@Controller
public class AnnonceController {

    @Autowired
    private AnnonceService annonceService;

    @Autowired
    private SecteurService secteurService;

    @GetMapping("/annonces")
    public String index(Model model) {
        Iterable<Annonce> annonces = annonceService.getAnnonces();
        model.addAttribute("annonces", annonces);

        return "annonces/index";
    }

    @GetMapping("/annonces/create")
    public String create(Model model) {
        Annonce annonce = new Annonce();
        Iterable<Secteur> secteurs = secteurService.getSecteurs();
        model.addAttribute("annonce", annonce);
        model.addAttribute("secteurs", secteurs);

        return "annonces/create";
    }

    @PostMapping("/annonces/store")
    public String store(@ModelAttribute("annonce") Annonce secteur) {
        annonceService.saveAnnonce(secteur);

        return "redirect:/annonces";
    }

    @GetMapping("/annonces/edit/{id}")
    public ModelAndView edit(@PathVariable(name = "id") Long id) {
        ModelAndView mav = new ModelAndView("annonces/edit");
        Optional<Annonce> annonce = annonceService.getAnnonce(id);
        mav.addObject("annonce", annonce);

        return mav;
    }

    @GetMapping("/annonces/delete/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        annonceService.deleteAnnonce(id);
        return "redirect:/annonces";
    }


}
