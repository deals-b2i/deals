package com.nickdev.deals.controller;

import com.nickdev.deals.model.Prestataire;
import com.nickdev.deals.repository.PrestataireRepository;
import com.nickdev.deals.service.SecteurService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Data
@Controller
public class AuthController {

    @Autowired
    private PrestataireRepository prestataireRepository;

    @GetMapping("/register")
    public String showRegistrationForm(Model model) {
        model.addAttribute("prestataire", new Prestataire());

        return "auth/register";
    }

    @PostMapping("/register")
    public String processRegister(Prestataire prestataire) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(prestataire.getPassword());
        prestataire.setPassword(encodedPassword);

        prestataireRepository.save(prestataire);

        return "auth/register";
    }
}
