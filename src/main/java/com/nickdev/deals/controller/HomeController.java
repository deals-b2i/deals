package com.nickdev.deals.controller;

import com.nickdev.deals.model.Annonce;
import com.nickdev.deals.model.Secteur;
import com.nickdev.deals.service.AnnonceService;
import com.nickdev.deals.service.SecteurService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Data
@Controller
public class HomeController {

    @Autowired
    private SecteurService secteurService;

    @Autowired
    private AnnonceService annonceService;

    @GetMapping("/")
    String index(Model model) {
        Iterable<Secteur> secteurs = secteurService.getSecteurs();
        Iterable<Annonce> annonces = annonceService.getAnnonces();
        model.addAttribute("secteurs", secteurs);
        model.addAttribute("annonces", annonces);

        return "index";
    }
}
