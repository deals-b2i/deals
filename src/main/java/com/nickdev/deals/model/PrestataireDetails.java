package com.nickdev.deals.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class PrestataireDetails implements UserDetails {

    private Prestataire prestataire;

    public PrestataireDetails(Prestataire prestataire) {
        this.prestataire = prestataire;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return prestataire.getPassword();
    }

    @Override
    public String getUsername() {
        return prestataire.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getFullName() {
        return prestataire.getNom() + " " + prestataire.getPrenoms();
    }
}
