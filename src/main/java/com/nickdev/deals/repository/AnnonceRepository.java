package com.nickdev.deals.repository;

import com.nickdev.deals.model.Annonce;
import org.springframework.data.repository.CrudRepository;

public interface AnnonceRepository extends CrudRepository<Annonce, Long> {
}
