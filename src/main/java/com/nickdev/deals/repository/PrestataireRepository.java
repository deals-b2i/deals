package com.nickdev.deals.repository;

import com.nickdev.deals.model.Prestataire;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface PrestataireRepository extends CrudRepository<Prestataire, Long> {

    @Query("SELECT u FROM Prestataire u WHERE u.email = ?1")
    public Prestataire findByEmail(String email);
}
