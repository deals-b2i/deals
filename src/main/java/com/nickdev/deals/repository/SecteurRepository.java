package com.nickdev.deals.repository;

import com.nickdev.deals.model.Secteur;
import org.springframework.data.repository.CrudRepository;

public interface SecteurRepository extends CrudRepository<Secteur, Long> {
}
