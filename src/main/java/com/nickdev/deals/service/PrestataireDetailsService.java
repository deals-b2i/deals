package com.nickdev.deals.service;

import com.nickdev.deals.model.Prestataire;
import com.nickdev.deals.model.PrestataireDetails;
import com.nickdev.deals.repository.PrestataireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class PrestataireDetailsService implements UserDetailsService {

    @Autowired
    private PrestataireRepository prestataireRepository;

    @Override
    public PrestataireDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Prestataire prestataire = prestataireRepository.findByEmail(username);
        if (prestataire == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return new PrestataireDetails(prestataire);
    }
}
