package com.nickdev.deals.service;

import com.nickdev.deals.model.Secteur;
import com.nickdev.deals.repository.SecteurRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Data
@Service
public class SecteurService {

    @Autowired
    private SecteurRepository secteurRepository;

    public Optional<Secteur> getSecteur(final Long id) {
        return secteurRepository.findById(id);
    }

    public Iterable<Secteur> getSecteurs() {
        return secteurRepository.findAll();
    }

    public void deleteSecteur(final Long id) {
        secteurRepository.deleteById(id);
    }

    public Secteur saveSecteur(Secteur Secteur) {
        Secteur savedSecteur = secteurRepository.save(Secteur);
        return savedSecteur;
    }
}
